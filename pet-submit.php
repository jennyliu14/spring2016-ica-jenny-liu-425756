<?php
session_start();

require 'database.php';

if(isset($_SESSION['pet_id'])) {

	$id = $_SESSION['pet_id'];
	$name = $_POST['post_name'];
	$species = $_POST['post_species'];
	$weight = $_POST['post_weight'];
	$description = $_POST['post_desc'];
	$filename = $_POST['filename'];

	$stmt = $mysqli->prepare("insert into pets (id, name, species, weight, description, filename) values (?, ?, ?, ?, ?, ?) ");

	if(!$stmt){
		printf("Query Prep Failed: %s\n", $mysqli->error);
		exit;
	}

	$stmt->bind_param('dsssss', $id, $name, $species, $weight, $description, $filename);

	$stmt->execute();

	$stmt->close();

//------------------------------------------------------------
//	SAVING FILE TO SERVER
//------------------------------------------------------------

//saves and stores file into full_path 
$full_path = "/home/jennyliu/public_html";

//Back to Comments
	echo ("Post added! <form name='backtohomepage' method='POST' action='pet-listings.php'>
		<input type='submit' value='Go to Pet Listings'/>
		</form>");
}
 
else {
		Header("Location: error.html");//session user and user who posted not the same
		}
?>